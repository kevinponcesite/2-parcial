const xhttp = new XMLHttpRequest();
xhttp.open('GET', 'ejemplo.json', true);
xhttp.send();
xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        const datos = JSON.parse(this.responseText);
        const res = document.querySelector('#datos');
        res.innerHTML = '';
        for (const i of datos) {
            res.innerHTML += `
            <tr>
                <td>${i.cedula}</td>
                <td>${i.nombre}</td>
                <td>${i.direccion}</td>
                <td>${i.telefono}</td>
                <td>${i.correo}</td>
                <td>${i.curso}</td>
                <td>${i.paralelo}</td>
            </tr>`
        }
    }
}
